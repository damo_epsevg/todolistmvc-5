package edu.upc.damo.toDoList;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;

import static java.lang.Boolean.TRUE;

public class MainActivity extends Activity {
    Boolean b = TRUE;
    private Button boto;
    private ListViewMeu listView;
    private ModelObservable model;
    private AdapterMeu adaptador;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.llista);

        inicialitza();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private void onClic() {
        if (b)
            model.add("X");
        else
            model.add("Y");
        b = !b;
    }




    private void inicialitza() {

        Model modelReal = new Model(this);
        model = new ModelObservable(modelReal);

        listView = findViewById(R.id.llista);
        adaptador = new AdapterMeu(this, android.R.layout.simple_list_item_1, android.R.id.text1, modelReal);
        listView.setAdapter(adaptador);

        boto = findViewById(R.id.button);
        boto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClic();
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                // Esborra l'objecte polsat
                model.del(position);
                return true;
            }
        });

    }
}
