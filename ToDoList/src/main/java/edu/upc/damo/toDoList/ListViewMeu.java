package edu.upc.damo.toDoList;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListAdapter;
import android.widget.ListView;

/**
 * Created by Josep M on 04/11/2015.
 */
public class ListViewMeu extends ListView
        implements ModelObservable.OnCanviModelListener {

    // Constructiors exigits pel framework

    public ListViewMeu(Context context, AttributeSet attribute, int defStyleAtt, int defStyleRes) {
        super(context, attribute, defStyleAtt, defStyleRes);
    }

    public ListViewMeu(Context context, AttributeSet attribute, int defStyleAtt) {
        super(context, attribute, defStyleAtt);
    }

    public ListViewMeu(Context context, AttributeSet attribute) {
        super(context, attribute);
    }

    @Override
    public void setAdapter(ListAdapter adapter) {
        super.setAdapter(adapter);
        ModelObservable modelObservable = new ModelObservable(((AdapterMeu) adapter).getModel());
        modelObservable.setOnCanviModelListener(this);
    }

    @Override
    public void onNovesDades() {
        invalidateViews();
    }

}
